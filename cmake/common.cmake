if(DEFINED COMMON_CMAKE_8609A96BCFA00463C47B040ED3AA311E)
    return()
endif()
set(COMMON_CMAKE_8609A96BCFA00463C47B040ED3AA311E 1)

set(API_IDENTITY_VERSION "1.1.0")
message(STATUS "API_IDENTITY_VERSION: ${API_IDENTITY_VERSION}")

set(PROTOS_DIR ${CMAKE_CURRENT_LIST_DIR}/..)
set(PROTOBUF_IMPORT_DIRS ${PROTOS_DIR})
set(FRED_IDENTITY_GRPC_FILES
    ${PROTOS_DIR}/fred_api/identity/service_identity_checker_grpc.proto
    ${PROTOS_DIR}/fred_api/identity/service_identity_manager_grpc.proto
    ${PROTOS_DIR}/fred_api/identity/service_identity_user_grpc.proto)

set(FRED_IDENTITY_PROTO_FILES
    ${PROTOS_DIR}/fred_api/identity/common_types.proto
    ${FRED_IDENTITY_GRPC_FILES})

set(FRED_IDENTITY_GENERATE_INTO ${CMAKE_CURRENT_BINARY_DIR})

set(PROTOBUF_PROTOC_OPTIONS --experimental_allow_proto3_optional)
set(GRPC_PROTOC_OPTIONS --experimental_allow_proto3_optional)
