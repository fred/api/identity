syntax = "proto3";

import "fred_api/identity/common_types.proto";

package Fred.Identity.Api;

message CheckCurrentContactDataRequest
{
    ContactId contact_id = 1; // mandatory; if missing, implementation throws MissingContactId exception
    ContactHistoryId contact_history_id = 2; // must either be unset or point to the current "history", otherwise it will throw a HistoryIdMismatch exception
    string auth_info = 3; // an empty string represents missing value
    string name = 4; // an empty string has no special meaning
    PlaceAddress address = 5; // if used, all address entries are considered filled in
    Date birthdate = 6;
}

message CheckCurrentContactDataReply
{
    oneof DataOrException
    {
        Data data = 1;
        Exception exception = 2;
    }
    message Data
    {
        ContactHistoryId contact_history_id = 1; // current contact "history"
        bool auth_info_matches = 2;
        optional string name = 3; // if present, it contains a value stored in the registry that is different from the value provided by the identity provider
        PlaceAddress address = 4; // present only if at least one address field differs from the address provided by the identity provider
        oneof DateMismatch
        {
            Date birthdate = 5; // present only if the value differs from the value provided by the identity provider and the value in registry has valid format
            string broken_birthdate = 6; // present only if the value in registry is broken (and naturally differs ...)
        }
    }
    message Exception
    {
        oneof Reasons
        {
            ContactDoesNotExist contact_does_not_exist = 1;
            HistoryIdMismatch history_id_mismatch = 2;
            MissingContactId missing_contact_id = 3;
            ContactIsOrganization contact_is_organization = 4;
        }
    }
}

service IdentityChecker
{
    // Returns registry contact's data that is different from identity contact's data.
    rpc check_current_contact_data (CheckCurrentContactDataRequest) returns (CheckCurrentContactDataReply) {}
}
