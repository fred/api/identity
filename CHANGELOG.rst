ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

1.1.0 (2024-05-21)
------------------------

* Add ``ContactIsOrganization`` exception

1.0.1 (2022-03-30)
------------------

* Fix package build with pyproject.toml

1.0.0 (2022-03-04)
------------------

* Fix cmake build (use ``pkg-config``)
* Major version bump

0.3.1 (2022-03-30)
------------------

* Fix project build with pyproject.toml

0.3.0 (2021-11-02)
------------------

* Update ``attach_identity``.
* Add ``update_identity``.
* Update project setup.

0.1.0 (2021-06-17)
------------------

Initial version.
